#include "character.h"
#include "display.h"
#include <cmath>

const std::string srcShield = "Assets/Textures/Animations/Shield.png";
extern Timer *timestep;
extern Tile ***board;
extern int tileSize;

Character::Character(std::string path, int size, int a):
	Object(path), target(NULL), path(path) {
	updateInGame(0, 0, size, size);
	updateSource(0, 0, 60, 60);
	updateOnScreen(1, 1, size, size);
	updateHeight(2);

	shieldCapacity = 25;
	shieldMaxCapacity = 25;
	shieldRecharge = 3;
	shieldRate = new Timer();
	shieldActivated = false;
	shield = new Object(srcShield);
	shield->updateSource(0, 0, 64, 64);
	shield->updateInGame(0, 0, size + 4, size + 4);

	speed = 224;
	offsetSpeed = sqrt((speed * speed) / 2);
	velX = 0;
	velY = 0;
	alignment = a;
	dashing = false;
	keepMoving = false;
	dashTime = new Timer();
	dashTime->start();
}

void Character::getInput(int x, int y, Object *t) {
	if (x != 0 && y != 0) {
		velX += x * offsetSpeed;
		velY += y * offsetSpeed;
	} else {
		velX += x * speed;
		velY += y * speed;
	}
	if (t) {
		target = t;
		pointTo(*target);
	}
}

void Character::stopDashing() {
	switch (d) {
		case north:
			velY += 1024; break;
		case east:
			velX -= 1024; break;
		case south:
			velY -= 1024; break;
		case west:
			velX += 1024; break;
		case northeast:
			velX -= 1024;
			velY += 1024; break;
		case southeast:
			velX -= 1024;
			velY -= 1024; break;
		case southwest:
			velX += 1024;
			velY -= 1024; break;
		case northwest:
			velX += 1024;
			velY += 1024; break;
	}
	dashing = false;
}

bool Character::checkMovement(Object &obstacle) {
	float time = timestep->getTime() / 1000.f;
	int offset = inGame->w / 8;
	Object temp;
	temp.updateInGame(inGame->x + velX * time + offset,
					inGame->y + velY * time + offset,
					inGame->w - 2 * offset, inGame->h - 2 * offset);
	bool collided = temp.collision(obstacle);
	return collided;
}

void Character::applyMovement() {
	move(velX * (timestep->getTime() / 1000.f),
		velY * (timestep->getTime() / 1000.f));
}

void Character::update(std::vector<Object *> *canCollide) {
	for (int i = 0; i < gun.size(); i++) {
		gun[i]->update();
	}
	if (shieldCapacity > 0) {
		if (shieldActivated) {
			float time = shieldRate->getTime() / 1000.f;	
			if (time >= 0.5) {
				shieldCapacity += shieldRecharge;
				shieldRate->start();
			}
			if (shieldCapacity >= shieldMaxCapacity) {
				shieldActivated = false;
				shieldCapacity = shieldMaxCapacity;
			}
		}
		if (target) {
			pointTo(*target);
		}
		float time = dashTime->getTime() / 1000.f;
		if (dashing && time >= 0.1) {
			stopDashing();
			dashTime->start();
		}
	} else {
		updateHeight(5);
	}
	keepMoving = false;
	bool canMove = true;
	std::vector<Object *>::iterator it;
	for (it = canCollide->begin(); it < canCollide->end(); ++it) {
		if (target && this != *it) {
			for (int i = 0; i < gun.size(); i++) {
				gun[i]->checkCollision(**it);
			}
		}
		if (shieldCapacity > 0) {
			if (checkMovement(**it) && this != *it && (*it)->getHeight() == 2) {
				if ((*it)->alignment == alignment) {
					keepMoving = true;
				} else {
					canMove = false;
					if (dashing) {
						stopDashing();
						dashTime->start();
					}
				}
			}
		}
	}
	if (shieldCapacity > 0) {
		Tile *t = getPresentTile();
		for (int i = t->x - 1; i <= t->x + 1; i++) {
			for (int j = t->y - 1; j <= t->y + 1; j++) {
				if (checkMovement(*board[i][j]) && board[i][j]->getHeight() == 2) {
					canMove = false;
					if (dashing) {
						stopDashing();
						dashTime->start();
					}
				}
			}
		}
		if (canMove) {
			applyMovement();
		}
		if (target) {
			for (int i = 0; i < gun.size(); i++) {
				if (shooting[i] && !collision(*target)) {
					Object temp1;
					temp1.updateInGame(inGame->x, inGame->y);
					Object temp2;
					temp2.updateInGame(0, 0, 0, 0);
					temp2.moveToDestCenter(*target);
					gun[i]->shoot(temp1, temp2);
				}
			}
		}
	}
}

void Character::render() {
	for (int i = 0; i < gun.size(); i++) {
		gun[i]->render();
	}
	if (shieldCapacity > 0) {
		Object::render();
		shield->updateInGame(inGame->x - 2, inGame->y - 2);
		if (shieldActivated) {
			shield->rotate(45);
			shield->render();
		}
	}
}

bool Character::collision(Object &o) {
	bool collide = Object::collision(o);
	if (collide && o.getHeight() == 3) {
		shieldActivated = true;
		shieldCapacity -= o.damage;
		shieldRate->start();	
	}
	return collide;
}

Tile *Character::getPresentTile() {
	int x = (inGame->x + (inGame->w / 2)) / tileSize;
	int y = (inGame->y + (inGame->h / 2)) / tileSize;
	return board[x][y];
}

void Character::shoot(int k) {
	shooting[k] = true;
}

void Character::stop(int k) {
	if (k == -1) {
		for (int i = 0; i < gun.size(); i++) {
			shooting[i] = false;
			gun[i]->reset();
		}
	} else {
		shooting[k] = false;
		gun[k]->reset();
	}
}

void Character::dash() {
	float time = dashTime->getTime() / 1000.f;
	if (!dashing && time >= 1.5) {
		if (velX > 0 && velY == 0) {
			velX += 1024;
			d = east;
		} else if (velX < 0 && velY == 0) {
			velX -= 1024;
			d = west;
		} else if (velX == 0 && velY < 0) {
			velY -= 1024;
			d = north;
		} else if (velX == 0 && velY > 0) {
			velY += 1024;
			d = south;
		} else if (velX > 0 && velY > 0) {
			velX += 1024;
			velY += 1024;
			d = southeast;
		} else if (velX < 0 && velY > 0) {
			velX -= 1024;
			velY += 1024;
			d = southwest;
		} else if (velX > 0 && velY < 0) {
			velX += 1024;
			velY -= 1024;
			d = northeast;
		} else if (velX < 0 && velY < 0) {
			velX -= 1024;
			velY -= 1024;
			d = northwest;
		}
		dashing = true;
		dashTime->start();
	}
}

bool Character::checkKeepMoving() {
	return keepMoving;
}

bool Character::isDead() {
	for (int i = 0; i < gun.size(); i++) {
		if (!gun[i]->empty()) {
			return false;
		}
	}
	if (shieldCapacity <= 0) {
		if (alignment == 1) {
			Display *d = Display::getInstance();
			d->updateShield(0);
		}
		return true;
	}
	return false;
}
