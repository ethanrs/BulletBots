#ifndef __STATUSCOMPONENT_H__
#define __STATUSCOMPONENT_H__
#include "DamageComponent.h"
#include "Timer.h"

class StatusComponent {
private:
	int maxShieldCap;
	int shieldCap;
	int shieldRecharge;
	Timer *clock;
public:
	StatusComponent(int);
	void takeDamage(DamageComponent *);
	void update();
	bool fullShield();
	bool isDead();
};

#endif

