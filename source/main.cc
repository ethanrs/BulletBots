#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <ctime>
#include "level.h"
#include "object.h"

using namespace std;

const int screenWidth = 1280;
const int screenHeight = 768;

SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;
SDL_Renderer *Object::renderer;
SDL_Renderer *Level::renderer;

int main(int, char**) {
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("Bullet Bots", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight - 10, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1,/* SDL_RENDERER_ACCELERATED | */SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderTarget(renderer, NULL);	
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	Object::renderer = renderer;
	Level::renderer = renderer;
	int img = IMG_INIT_PNG;
	IMG_Init(img);		
	SDL_ShowCursor(SDL_DISABLE);

	int seed = time(NULL);
//	int seed = 19;
	Level *one = new Level(seed);
	one->loop();
	delete one;

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();
}
