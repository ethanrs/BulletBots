#include "tile.h"
#include <utility>
#include <algorithm>
#include <map>
#include <vector>
#include <stack>
#include <iostream>

extern Tile ***board;

Tile::Tile(std::string path, int x, int y, int size, bool border, bool open):
Object(path), border(border), open(open), x(x), y(y) {
	updateInGame(x * size, y * size, size, size);
	if (open) {
		updateHeight(1);
	} else {
		updateHeight(2);
	}
}

bool Tile::isOpen() {
	return open;
}

bool Tile::isBorder() {
	return border;
}
