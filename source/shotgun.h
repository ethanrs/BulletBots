#ifndef __SHOTGUN_H__
#define __SHOTGUN_H__
#include <vector>
#include <string>
#include "arm.h"
#include "object.h"
#include "bullet.h"
#include "timer.h"

class Shotgun : public Arm {
	private:
		int damage;
		int alignment;	
		std::vector<Bullet *> bullets;
		Timer *fireRate;
		bool ready;

	public:
		Shotgun(int);
		~Shotgun();
		void shoot(Object&, Object&);
		void checkCollision(Object&);
		void reset();
		void update();
		void render();
		bool empty();
};

#endif
