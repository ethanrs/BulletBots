#include "Input.h"
#include "Player.h"
#include "Reticle.h"
#include <iostream>

bool Input::instanceFlag = false;
Input *Input::instance = NULL;

Input *Input::getInstance() {
	if (!instanceFlag) {
		instance = new Input();
		instanceFlag = true;
	}
	return instance;
}

Input::Input() {
	quit = false;
}

Input::~Input() {
	instanceFlag = false;
}

void Input::process() {
	while(SDL_PollEvent(&e) != 0) {
		int x = 0;
		int y = 0;
		int motionX = -1;
		int motionY = -1;
		Player *player = Player::getInstance();
		if (e.type == SDL_QUIT) {
			quit = true;
		}
		if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
			switch (e.key.keysym.sym) {
				case SDLK_w: y--; break;
				case SDLK_s: y++; break;
				case SDLK_a: x--; break;
				case SDLK_d: x++; break;
				case SDLK_SPACE: player->activateSpecial(); break;	
			}
		} else if (e.type == SDL_KEYUP && e.key.repeat == 0) {
			switch (e.key.keysym.sym) {
				case SDLK_w: y++; break;
				case SDLK_s: y--; break;
				case SDLK_a: x++; break;
				case SDLK_d: x--; break;
			}
		}
		if (e.button.state == SDL_PRESSED &&
			e.button.button == SDL_BUTTON_LEFT) {
			player->shootLeft();
		}
		if (e.button.state == SDL_RELEASED &&
			e.button.button == SDL_BUTTON_LEFT) {
			player->stopLeft();
		}
		if (e.button.state == SDL_PRESSED &&
			e.button.button == SDL_BUTTON_RIGHT) {
			player->shootRight();
		}
		if (e.button.state == SDL_RELEASED &&
			e.button.button == SDL_BUTTON_RIGHT) {
			player->stopRight();
		}
		if (e.type == SDL_MOUSEMOTION) {
			motionX = e.motion.x;
			motionY = e.motion.y;
			if (SDL_GetMouseState(NULL, NULL) == SDL_BUTTON(1)) {
				player->shootLeft();
			}
			if (SDL_GetMouseState(NULL, NULL) == SDL_BUTTON(3)) {
				player->shootRight();
			}
		}
		Reticle::getInstance()->processInput(motionX, motionY);
		player->processInput(x, y);
		player->changeTarget(Reticle::getInstance()->getSpace());
	}
}

bool Input::active() {
	return !quit;
}
