#include "ShotGun.h"
#include "Character.h"
#include <cmath>
#include <utility>
#include <iostream>

const float pi = 3.14159265358;
const std::string src = "Assets/Guns/Shotgun.png";

ShotGun::ShotGun(PhysicalComponent *space, PhysicalComponent *target):
GunComponent(space, target) {
	for (int i = 0; i < 6; i++) {
		HitscanComponent *r = new HitscanComponent();
		rays.push_back(r);
	}
	damage = new DamageComponent(10);
	image = new GraphicalComponent(src);
	std::pair<int,int> pos = space->getPosition();
	image->updateBounds(pos.first, pos.second, 50, 50);
	addHeat = 2;
}

void ShotGun::update(std::pair<int,int> offset) {
	GunComponent::update(offset);
	bool displayed = false;
	std::vector<HitscanComponent *>::iterator it;
	for (it = rays.begin(); it < rays.end(); ++it) {
		displayed = (*it)->display() || displayed;
	}
	if (activated && !displayed) {
		float time = fireRate->getTime() / 1000.f;
		if (time == 0 || time > 0.5) {
			fireRate->start();
			std::pair<int,int> posTarget = target->getPosition();
			std::pair<int,int> posSpace = space->getPosition();
			std::pair<float,float> diff = target->getDifference(*space);
			float hypotenuse = sqrt(pow(diff.second, 2) + pow(diff.first, 2));
			PhysicalComponent temp = PhysicalComponent(*target);
			std::vector<HitscanComponent *>::iterator it;
			for (it = rays.begin(); it < rays.end(); ++it) {
				float tempAngle =  atan2(std::abs(posTarget.second - posSpace.second),
									std::abs(posTarget.first - posSpace.first));
				float random = rand() % 700;
				float angle = tempAngle - 0.35 + random / 1000;
				float x = cos(angle) * hypotenuse;
				float y = sin(angle) * hypotenuse;
				if (posTarget.first <= posSpace.first) {
					x *= -1;
				}
				if (posTarget.second <= posSpace.second) {
					y *= -1;
				}
				temp.changePosition(posTarget.first + x, posTarget.second + y);
				(*it)->shoot(space, &temp, offset, damage);
				heatUp();
			}
		}
		activated = false;
	}
}
