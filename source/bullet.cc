#include "bullet.h"
#include "timer.h"
#include "display.h"
#include <cmath>

const int size = 8;
const int speed = 768;
extern Timer *timestep;
extern Tile ***board;
extern int tileSize;

Bullet::Bullet(): Object() {}

Bullet::Bullet(Object &s, Object &t, int d): Object() {
	damage = d;
	Bullet *b = new Bullet();
	b->updateInGame(0, 0, 0, 0);
	b->moveToDest(t);
	targetX = b->inGame->x + (b->inGame->w / 2);
	targetY = b->inGame->y + (b->inGame->h / 2);
	b->setScreenPos();
	updateInGame(0, 0, size, size);
	moveToDest(s);
	move(26, 26);
	updateSource(0, 0, size, size);
	updateHeight(3);
	setScreenPos();
	pointTo(*b);
	delete b;
	targetReached = false;
	velX = 0;
	velY = 0;
}

void Bullet::update() {
	if (targetX >= inGame->x - size && targetX <= inGame->x + size &&
		targetY >= inGame->y - size && targetY <= inGame->y + size) {
		targetReached = true;
	}
	if (!targetReached) {
		float x = targetX - inGame->x;
		float y = targetY - inGame->y;
		float angle = atan(std::abs(y)/ std::abs(x));
		velX = speed * cos(angle);
		velY = speed * sin(angle);
		if (targetX <= inGame->x) {
			velX *= -1;
		}
		if (targetY <= inGame->y) {
			velY *= -1;
		}
	}
}

void Bullet::applyMovement() {
	move(velX * (timestep->getTime() / 1000.f),
		velY * (timestep->getTime() / 1000.f));
}

Tile *Bullet::getPresentTile() {
	int x = (inGame->x + (inGame->w / 2)) / tileSize;
	int y = (inGame->y + (inGame->h / 2)) / tileSize;
	return board[x][y];
}
