#include "GameObject.h"

GameObject::GameObject() {
	space = NULL;
	image = NULL;
}

GameObject::~GameObject() {
	delete space;
	delete image;
}

PhysicalComponent *GameObject::getSpace() {
	return space;
}
