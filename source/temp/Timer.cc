#include "Timer.h"

Timer::Timer() {
	timeStart = 0;
}

Uint32 Timer::getTime() {
	int current = SDL_GetTicks();
	return current - timeStart;
}

void Timer::start() {
	timeStart = SDL_GetTicks();
}
