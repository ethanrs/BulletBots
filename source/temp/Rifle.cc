#include "Rifle.h"
#include "Character.h"
#include <utility>
#include <iostream>

const std::string src = "Assets/Guns/Rifle.png";

Rifle::Rifle(PhysicalComponent *space, PhysicalComponent *target):
GunComponent(space, target) {
	for (int i = 0; i < 2; i++) {
		HitscanComponent *r = new HitscanComponent();
		rays.push_back(r);
	}
	damage = new DamageComponent(15);
	image = new GraphicalComponent(src);
	std::pair<int,int> pos = space->getPosition();
	image->updateBounds(pos.first, pos.second, 50, 50);
	addHeat = 3;
}

void Rifle::update(std::pair<int,int> offset) {	
	GunComponent::update(offset);
	bool displayed = false;
	std::vector<HitscanComponent *>::iterator it;
	for (it = rays.begin(); it < rays.end(); ++it) {
		displayed = (*it)->display() || displayed;
	}
	if (activated && !displayed) {
		float time = fireRate->getTime() / 1000.f;
		if (time == 0 || time > 0.1) {
			fireRate->start();
			std::pair<int,int> posTarget = target->getPosition();
			PhysicalComponent temp = PhysicalComponent(*target);
			std::vector<HitscanComponent *>::iterator it;
			for (it = rays.begin(); it < rays.end(); ++it) {
				temp.changePosition(posTarget.first, posTarget.second);
				(*it)->shoot(space, &temp, offset, damage);
				heatUp();
			}
		}
		activated = false;
	}
}
