#ifndef __ARM_H__
#define __ARM_H__
#include "object.h"
#include "item.h"
#include "timer.h"
#include <vector>

class Arm {
	protected:
		int heatMaxCapacity;
		int heatCapacity;
		Timer *heatTime;
		bool disabled;
		Item *dropped;

	public:
		Arm();
		virtual void shoot(Object&, Object&) = 0;
		virtual void checkCollision(Object&) = 0;
		virtual void reset() = 0;
		virtual void render() = 0;
		virtual bool empty() = 0;
		virtual void update();
		float getRatio();
		void drop(Object&);
		void grabbed();
		bool overheated();
		Item *getDropped();
};

#endif
