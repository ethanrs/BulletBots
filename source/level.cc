#include "level.h"
#include <iostream>

static SDL_Renderer *renderer;
const int screenWidth = 1280;
const int screenHeight = 768;
const int boardWidth = screenWidth * 8;
const int boardHeight = screenHeight * 8;
const int borderSize = screenWidth;
int tileSize = screenHeight / 14;
const std::string source1 = "Assets/Textures/Tiles/FloorTileSet1.png";
const std::string source2 = "Assets/Textures/Tiles/FloorTileSet2.png";
const std::string source3 = "Assets/Textures/Tiles/FloorTileSet3.png";

Timer *timestep;
Tile ***board;

Level::Level(int seed): seed(seed) {
	srand(seed);
	board = new Tile**[(boardWidth + (borderSize * 2)) / tileSize];
	for (int i = 0; i < (boardWidth + (borderSize * 2)) / tileSize; i++) {
		board[i] = new Tile*[(boardHeight + (borderSize * 2)) / tileSize];
		for (int j = 0; j < (boardHeight + (borderSize * 2)) / tileSize; j++) {
			board[i][j] = NULL;
			createTile(i, j, true);
		}
	}
	int type = rand() % 3;
	int x = 0;
	int y = 0;
	int direction = -1;
	while(!generateRoom(x, y, direction)) {
		x = rand() % (boardWidth / tileSize - 30) + (borderSize / tileSize + 30);
		y = rand() % (boardHeight / tileSize - 30) + (borderSize / tileSize + 30);
		direction = rand() % 4;
	}

	player = new Player(tileSize - 4);
	player->moveToDest(*spawnPoints[0]);
	spawnPoints.erase(spawnPoints.begin());
	Control *con = Control::getInstance();
	con->addTarget(player);
	Input *in = Input::getInstance();
	in->addPlayer(player);

	std::vector<Object *>::iterator it;
	for (it = spawnPoints.begin(); it < spawnPoints.end(); ++it) {
		int r = rand() % 9;
		Arm *a = NULL;
		if (r < 3) {
			if (r == 0) {
				a = new Machinegun(1);
				items.push_back(a);
			} else if (r == 1) {
				a = new Shotgun(1);
				items.push_back(a);
			} else if (r == 2) {
				a = new Pistol(1);
				items.push_back(a);
			}
			a->drop(**it);
		} else {
			Enemy *e = new Enemy(tileSize - 4);
			e->moveToDest(**it);
			enemies.push_back(e);
		}
	}
	spawnPoints.clear();
}

Level::~Level() {
	for (int i = 0; i < (boardWidth + (borderSize * 2)) / tileSize; i++) {
		for (int j = 0; j < (boardHeight + (borderSize * 2)) / tileSize; j++) {
			delete board[i][j];
		}
		delete [] board[i];
	}
	delete [] board;
}

bool Level::generateRoom(int x, int y, int direction) {
	int w = rand() % 20 + 8;
	int h = rand() % 20 + 8;
	int startX = 0;
	int endX = 0;
	int startY = 0;
	int endY = 0;
	if (direction == 0) {
		startX = x - (w / 2);
		if (w % 2 == 1) {
			endX = x + (w / 2) + 1;
		} else {
			endX = x + (w / 2);
		}
		startY = y - h;
		endY = y;
	} else if (direction == 1) {
		startX = x;
		endX = x + w;
		startY = y - (h / 2);
		if (h % 2 == 1) {
			endY = y + (h / 2) + 1;
		} else {
			endY = y + (h / 2);
		}
	} else if (direction == 2) {
		startX = x - (w / 2);
		if (w % 2 == 1) {
			endX = x + (w / 2) + 1;
		} else {
			endX = x + (w / 2);
		}
		startY = y;
		endY = y + h;
	} else if (direction == 3) {
		startX = x - w;
		endX = x;
		startY = y - (h / 2);
		if (h % 2 == 1) {
			endY = y + (h / 2) + 1;
		} else {
			endY = y + (h / 2);
		}
	}

	if (!checkSpace(startX, startY, endX - startX, endY - startY)) {
		return false;
	}

	bool firstSpawn = false;
	if (spawnPoints.empty()) {
		firstSpawn = true;
	}

	for (int i = startX; i < endX; i++) {
		for (int j = startY; j < endY; j++) {
			createTile(i, j, false);
			if (!firstSpawn || spawnPoints.empty()) {
				int r = rand() % 15;
				if (r == 0) {
					if (!board[i][j]->isBorder() && board[i][j]->isOpen()) {
						spawnPoints.push_back(board[i][j]);
					}
				}
			}
		}
	}

	for (int i = 0; i < 4; i++) {
		if (direction != i) {
			if (i == 0) {
				generateTunnel(startX + (w / 2), startY, 0);
			} else if (i == 1) {
				generateTunnel(startX + w, startY + (h / 2), 1);
			} else if (i == 2) {
				generateTunnel(startX + (w / 2), startY + h, 2);
			} else if (i == 3) {
				generateTunnel(startX, startY + (h / 2), 3);
			}
		}
	}
	return true;
}

void Level::generateTunnel(int x, int y, int direction) {
	int h = rand() % 15 + 3;
	int w = 4;
	if (direction == 0 && generateRoom(x + w - 2, y - h, direction) &&
		checkSpace(x, y, w - 2, -h)) {
		for (int i = x - 2; i < x + w - 2; i++) {
			for (int j = y; j >= y - h; j--) {
				createTile(i, j, false);
			}
		}
	} else if (direction == 1 && generateRoom(x + h, y + w - 2, direction) &&
				checkSpace(x, y, h, w - 2)) {
		for (int i = x; i < x + h; i++) {
			for (int j = y - 2; j < y + w - 2; j++) {
				createTile(i, j, false);
			}
		}
	} else if (direction == 2 && generateRoom(x + w - 2, y + h, direction) &&
				checkSpace(x, y, w - 2, h)) {
		for (int i = x - 2; i < x + w - 2; i++) {
			for (int j = y; j < y + h; j++) {
				createTile(i, j, false);
			}
		}
	} else if (direction == 3 && generateRoom(x - h, y + w - 2, direction) &&
				checkSpace(x, y, -h, w - 2)) {
		for (int i = x; i >= x - h; i--) {
			for (int j = y - 2; j < y + w - 2; j++) {
				createTile(i, j, false);
			}
		}
	}
}

bool Level::checkSpace(int x, int y, int w, int h) {
	if (x + w >= (boardWidth + borderSize) / tileSize || x < borderSize / tileSize ||
		y + h >= (boardHeight + borderSize) / tileSize || y < borderSize / tileSize) {
		return false;
	}
	for (int i = x; i < x + w; i++) {
		for (int j = y; j < y + h; j++) {
			if (!board[i][j]->isBorder()) {
				return false;
			}
		}
	}
	return true;
}

void Level::createTile(int x, int y, bool border) {
	delete board[x][y];
	int r = rand() % 10;
	if (border) {
		board[x][y] = new Tile(source2, x, y, tileSize, border, false);
	} else {
		bool space = true;
		if (r == 0) {
			for (int i = x - 2; i <= x + 2; i++) {
				if ((!board[i][y - 2]->isOpen() && !board[i][y - 2]->isBorder()) ||
					(!board[i][y + 2]->isOpen() && !board[i][y + 2]->isBorder())) {
					space = false;
				}
			}
			for (int i = y - 2; i <= y + 2; i++) {
				if ((!board[x - 2][i]->isOpen() && !board[x - 2][i]->isBorder()) ||
					(!board[x + 2][i]->isOpen() && !board[x + 2][i]->isBorder())) {
					space = false;
				}
			}
		} else {
			space = false;
		}

		if (space) {
			board[x][y] = new Tile(source3, x, y, tileSize, border, false);
		} else {
			board[x][y] = new Tile(source1, x, y, tileSize, border, true);
		}
	}
}

void Level::loop() {
	Display *HUD = Display::getInstance(screenWidth, screenHeight);
	Input *in = Input::getInstance();
	Control *con = Control::getInstance();
	timestep = new Timer();
	timestep->start();

	int frames = 0;
	float rate = 0;

	while (in->active()) {
		if (player->isDead() || enemies.empty()) {
			break;
		}
		in->update();
		player->update(&onScreen);
		con->update(&onScreen);
		HUD->update(*player);


		onScreen.clear();
		std::vector<Arm *>::iterator iit;
		for (iit = items.begin(); iit < items.end(); ++iit) {
			if (player->collision(*(*iit)->getDropped())) {
				Arm *a = player->pickUp(*iit);
				if (a) {
					a->grabbed();
					items.erase(iit++);
				}
			}
			if (HUD->getScreen()->collision(*(*iit)->getDropped())) {
					onScreen.push_back((*iit)->getDropped());
			}
		}
		std::vector<Enemy *>::iterator eit;
		for (eit = enemies.begin(); eit < enemies.end(); ++eit) {
			if ((*eit)->isDead()) {
				con->remove(*eit);
				enemies.erase(eit);
			}
			if (HUD->getScreen()->collision(**eit)) {
				if (con->activated(*eit)) {
					con->addControlled(*eit);
				} else {
					onScreen.push_back(*eit);
				}
			}
		}
		onScreen.push_back(player);
		con->getControlled(&onScreen);

		float time = 1 / (timestep->getTime() / 1000.f);
		rate += time;
		frames++;
		timestep->start();


		SDL_SetRenderTarget(renderer, NULL);
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(renderer);
		for (int i = HUD->lowerX/tileSize; i < HUD->upperX/tileSize+1; i++) {
			for (int j = HUD->lowerY/tileSize; j < HUD->upperY/tileSize+1; j++) {
				if (HUD->getScreen()->collision(*(board[i][j]))) {
					board[i][j]->render();
				}
			}
		}
		std::vector<Object *>::iterator it;
		for (it = onScreen.begin(); it < onScreen.end(); ++it) {
			(*it)->render();
		}
		HUD->render();
		SDL_RenderPresent(renderer);
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);	
		SDL_RenderClear(renderer);
	}
	std::cout << rate / frames << std::endl;
}
