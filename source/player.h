#ifndef __PLAYER_H__
#define __PLAYER_H__
#include "character.h"
#include "object.h"
#include "arm.h"

class Player : public Character {
	private:
		int grabArm;

	public:
		Player(int);
		void update(std::vector<Object *> *canCollide);
		bool collision(Object &o);
		void switchPrimary();
		void switchSecondary();
		Arm *pickUp(Arm *a);
};

#endif
