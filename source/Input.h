#ifndef __INPUT_H__
#define __INPUT_H__
#include <SDL2/SDL.h>

class Input {
private:
	Input();
	static Input *instance;
	static bool instanceFlag;
	SDL_Event e;
	bool quit;
public:
	~Input();
	static Input *getInstance();
	bool active();
	void process();
};

#endif
