#include "shotgun.h"
#include "display.h"

extern Tile ***board;
const std::string srcBullet = "Assets/Textures/pistolBullet.png";

Pistol::Pistol(int a): Arm() {
	damage = 10;
	alignment = a;
	fireRate = new Timer();
	ready = true;
}

Pistol::~Pistol() {
	for (int i = 0; i < bullets.size(); i++) {
		delete bullets[i];
		bullets[i] = NULL;
	}
}

void Pistol::shoot(Object &s, Object &t) {
	float time = fireRate->getTime() / 1000.f;	
	if ((time == 0 || time > 0.25) && ready && !disabled) {
		Bullet *b = new Bullet(s, t, damage);
		b->updatePath(srcBullet);
		bullets.push_back(b);
		fireRate->start();
		ready = false;
		heatCapacity += 2;
		heatTime->start();
	}
	Arm::update();	
}

void Pistol::reset() {
	ready = true;
}

void Pistol::update() {
	Arm::update();
	std::vector<Bullet *>::iterator it;
	for (it = bullets.begin(); it < bullets.end(); ++it) {
		(*it)->update();
		(*it)->applyMovement();
		Tile *t = (*it)->getPresentTile();
		for (int i = t->x - 1; i <= t->x + 1; i++) {
			for (int j = t->y - 1; j <= t->y + 1; j++) {
				checkCollision(*board[i][j]);
			}
		}
	}
}

void Pistol::checkCollision(Object &obstacle) {
	Display *d = Display::getInstance();
	std::vector<Bullet *>::iterator it;
	for (it = bullets.begin(); it < bullets.end(); ++it) {
		if (((*it)->collision(obstacle) && obstacle.getHeight() == 2 &&
			obstacle.alignment != alignment) || !d->getScreen()->collision(**it)) {
			obstacle.collision(**it);
			bullets.erase(it);
			break;
		}
	}
}

void Pistol::render() {
	Display *d = Display::getInstance();
	std::vector<Bullet *>::iterator it;
	for (it = bullets.begin(); it < bullets.end(); ++it) {
		if (d->getScreen()->collision(**it)) {
			(*it)->render();
		}
	}
}

bool Pistol::empty() {
	return bullets.empty();
}
