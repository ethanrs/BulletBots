#ifndef __AICOMPONENT_H__
#define __AICOMPONENT_H__
#include "Character.h"
#include "Tile.h"
#include <stack>
#include <map>
#include <vector>

enum State {
	inactive,
	searching,
	shooting,
	clearing
};

class AIComponent {
private:
	Character *subject;
	State state;
	std::stack<Tile *> instructions;
	std::pair<int, int> lastInput;
	PhysicalComponent *target;
public:
	AIComponent(Character *);
	std::stack<Tile *> findPath(Tile *,Tile *);
	Tile *getFreeTile(Tile *);
	bool lineOfSight(PhysicalComponent*);
	bool clearShot(PhysicalComponent*);
	void moveToTarget();
	void getInput();
	bool onScreen();
	bool inCenter();
	void moveCenter();
	bool inRange();
};

#endif
