#ifndef __RETICLE_H__
#define __RETICLE_H__
#include "GraphicalComponent.h"
#include "GameObject.h"
#include "GunComponent.h"

class Reticle : public GameObject {
private:
	Reticle();
	static Reticle *instance;
	static bool instanceFlag;
	int playerX, playerY;
	GraphicalComponent *rightBar;
	GunComponent *rightGun;
	GraphicalComponent *leftBar;
	GunComponent *leftGun;
public:
	~Reticle();
	static Reticle *getInstance();
	void processInput(int,int);
	void update();
	void addRightGun(GunComponent *);
	void addLeftGun(GunComponent *);
};

#endif
