#ifndef __DIRECTIONCOMPONENT_H__
#define __DIRECTIONCOMPONENT_H__
#include "PhysicalComponent.h"
#include <utility>

class DirectionComponent {
private:
	PhysicalComponent *space;
	PhysicalComponent *target;
	std::pair<int,int> offset;
public:
	DirectionComponent(PhysicalComponent *,int=0,int=0);
	void changeTarget(PhysicalComponent *);
	void changeOffset(std::pair<int,int>);
	float getDirection();
};

#endif
