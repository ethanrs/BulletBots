#ifndef __DISPLAY_H__
#define __DISPLAY_H__
#include "object.h"
#include "character.h"

class Display {
	private:
		Display(int, int);
		static Display *instance;
		static bool instanceFlag;
		Object *cursor;
		Object *screen;
		Object *health;
		Object *primary;
		Object *secondary;

	public:
		~Display();
		static Display *getInstance(int=0, int=0);
		void setCursor(int, int);
		void render();
		void update(Character&);
		Object *getScreen();
		Object *getCursor();
		int lowerX, upperX;
		int lowerY, upperY;
		void updateShield(float);
		void updatePrimary(float);
		void updateSecondary(float);
};

#endif
