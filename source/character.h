#ifndef __CHARACTER_H__
#define __CHARACTER_H__
#include <string>
#include <vector>
#include "object.h"
#include "timer.h"
#include "arm.h"
#include "machinegun.h"
#include "shotgun.h"
#include "pistol.h"
#include "tile.h"

enum direction {
	north,
	east,
	south,
	west,
	northeast,
	southeast,
	southwest,
	northwest
};

class Character : public Object {
	protected:
		Object *target;
		std::string path;
		int level;

		int shieldCapacity;
		int shieldMaxCapacity;
		int shieldRecharge;
		bool shieldActivated;
		Object *shield;
		Timer *shieldRate;

		int speed;
		int offsetSpeed;
		int velX, velY;
		bool keepMoving;
		bool dashing;
		Timer *dashTime;
		direction d;
		void stopDashing();

		std::vector<bool> shooting;
		std::vector<Arm *> gun;
		bool checkMovement(Object&);
		void applyMovement();

	public:
		Character(std::string, int, int);
		virtual void update(std::vector<Object *>*);
		virtual bool collision(Object&);
		virtual void render();
		void getInput(int, int, Object* = NULL);
		void shoot(int);
		void stop(int=-1);
		void dash();
		bool checkKeepMoving();
		bool isDead();
		Tile *getPresentTile();
};

#endif
